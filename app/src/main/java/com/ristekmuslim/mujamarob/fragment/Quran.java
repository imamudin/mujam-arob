package com.ristekmuslim.mujamarob.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.ristekmuslim.mujamarob.DetailWord;
import com.ristekmuslim.mujamarob.R;
import com.ristekmuslim.mujamarob.adapter.CustomListAdapterArab;
import com.ristekmuslim.mujamarob.adapter.CustomListAdapterIndo;
import com.ristekmuslim.mujamarob.adapter.CustomListAdapterLisanulArab;
import com.ristekmuslim.mujamarob.db.DatabaseAccess2;
import com.ristekmuslim.mujamarob.db.QueryData;

import java.util.ArrayList;

/**
 * Created by imamudin on 17/01/19.
 */

public class Quran extends Fragment {

    private ArrayList ar_words = new ArrayList();
    private ArrayList data, wordsArab, wordsIndo;
    private ListView list_query;
    private CustomListAdapterLisanulArab adapter_arab;

    private char char_arab_terkecil = '\u0600', char_arab_terbesar ='\u06ff';
    View rootView;
    String word="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        rootView = inflater.inflate(R.layout.munawwir, container, false);

        rootView = inflater.inflate(R.layout.layout_listview, container, false);
        DatabaseAccess2 databaseAccess2 = DatabaseAccess2.getInstance(getContext());
        databaseAccess2.open();
        //databaseAccess2.getWordsIndo_Munawwir("belajar");

        return rootView;
    }
    public void TesToast(){
        Toast.makeText(getContext(), "Kamus Munawir ", Toast.LENGTH_SHORT).show();
        Log.d("sdfsdfsdf", "Sfsdfsdfsdfsdfsd");
    }


    public void callDatas(String word){
        new AsyncTaskRunner().execute(word);
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, ArrayList> {

        @Override
        protected ArrayList doInBackground(String... params) {
            //publishProgress("Sleeping..."); // Calls onProgressUpdate()
            word = params[0];

            if (!word.equals("") && word.length() > 0) {
                DatabaseAccess2 databaseAccess2 = DatabaseAccess2.getInstance(getContext());
                databaseAccess2.open();
                final boolean isArab = isArab(word.toLowerCase().charAt(0));

                if(!isArab){
                    ar_words = new ArrayList<QueryData>();
                    QueryData one = new QueryData();
                    one.setIndo("'<b>"+word+"</b>' not found");
                    ar_words.add(one);
                }else {
                    ar_words = databaseAccess2.getWordsArab_Quran(word.replace("'",""));
                }
                databaseAccess2.close();
            } else {
                ar_words = new ArrayList<QueryData>();
            }
            return ar_words;
        }

        @Override
        protected void onPostExecute(ArrayList ar_words) {
            // execution of result of Long time consuming operation
            if (word.length() > 0) {
                final boolean isArab = isArab(word.toLowerCase().charAt(0));
                list_query = (ListView)rootView.findViewById(R.id.lv_result);
                adapter_arab = new CustomListAdapterLisanulArab(getContext(), ar_words);

                list_query.setAdapter(adapter_arab);

                registerForContextMenu(list_query);
                list_query.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                        Object o = list_query.getItemAtPosition(position);
                        QueryData newsData = (QueryData) o;

                        Intent myIntentA1A2 = new Intent(getContext(), DetailWord.class);
                        Bundle myData = new Bundle();

                        myData.putBoolean("isArab", isArab);
                        myData.putString("arab", "" + newsData.getArab());
                        myData.putString("indo", newsData.getIndo());

                        myIntentA1A2.putExtras(myData);

                        startActivityForResult(myIntentA1A2, 103);
                    }
                });
                adapter_arab.notifyDataSetChanged();
            } else {
                list_query = (ListView)rootView.findViewById(R.id.lv_result);
                adapter_arab = new CustomListAdapterLisanulArab(getContext(), ar_words);

                list_query.setAdapter(adapter_arab);
                registerForContextMenu(list_query);
                adapter_arab.notifyDataSetChanged();
            }
//            swipeContainer.setRefreshing(false);
        }


        @Override
        protected void onPreExecute() {
//            swipeContainer.setRefreshing(true);
        }


        @Override
        protected void onProgressUpdate(String... text) {
//            finalResult.setText(text[0]);

        }
    }

/*

    public void callDatasx(String word){

        if (!word.equals("") && word.length() > 0) {
            DatabaseAccess2 databaseAccess2 = DatabaseAccess2.getInstance(getContext());
            databaseAccess2.open();

            final boolean isArab = isArab(word.toLowerCase().charAt(0));

            if(!isArab && word.length() > 0){
                data = new ArrayList<QueryData>();

                QueryData one = new QueryData();
                one.setIndo("'<b>"+word+"</b>' not found");
                data.add(one);

                list_query = (ListView)rootView.findViewById(R.id.lv_result);
                adapter_indo = new CustomListAdapterIndo(getContext(), data);
                list_query.setAdapter(adapter_indo);
                registerForContextMenu(list_query);
                adapter_indo.notifyDataSetChanged();

            }else if(isArab && word.length() > 0){
                wordsArab = databaseAccess2.getWordsArab_Quran(word.replace("'",""));
                list_query = (ListView)rootView.findViewById(R.id.lv_result);
                adapter_arab = new CustomListAdapterArab(getContext(), wordsArab);

                list_query.setAdapter(adapter_arab);

                registerForContextMenu(list_query);
                list_query.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                        Object o = list_query.getItemAtPosition(position);
                        QueryData newsData = (QueryData) o;

                        Intent myIntentA1A2 = new Intent(getContext(), DetailWord.class);
                        Bundle myData = new Bundle();

                        myData.putBoolean("isArab", isArab);
                        myData.putString("arab", "" + newsData.getArab());
                        myData.putString("indo", newsData.getIndo());

                        myIntentA1A2.putExtras(myData);

                        startActivityForResult(myIntentA1A2, 103);
                    }
                });
                adapter_arab.notifyDataSetChanged();
            }else{
                data = new ArrayList<QueryData>();
                list_query = (ListView)rootView.findViewById(R.id.lv_result);
                adapter_indo = new CustomListAdapterIndo(getContext(), data);

                list_query.setAdapter(adapter_indo);
                registerForContextMenu(list_query);
                adapter_indo.notifyDataSetChanged();
            }
            databaseAccess2.close();
        } else {
            data = new ArrayList<QueryData>();
            list_query = (ListView)rootView.findViewById(R.id.lv_result);
            adapter_indo = new CustomListAdapterIndo(getContext(), data);

            list_query.setAdapter(adapter_indo);
            registerForContextMenu(list_query);
            adapter_indo.notifyDataSetChanged();
        }
    }
*/

    private boolean isArab(char x){
        if(x > char_arab_terkecil){
            return  true;
        }
        return false;
    }
}