package com.ristekmuslim.mujamarob.config;

/**
 * Created by agung on 09/03/2016.
 */
public class GlobalConfig {
    public static final String ENVIRONMENT      = "development";   //production  -> log tidak muncul, development -> log muncul
    public static final String NAMA_PREF        = "com.muslimtekno.kamusarabindonesia.pref";  //key untuk menyimpan file preferences
    public static final String PREF_FIRST_QUERY = "first_query";  //key untuk menyimpan pertama kali search
    public static final String PREF_FONT_SIZE   = "font_size_index";  //key untuk menyimpan ukuran font

    public static final String TAG              = "kai";                  //key untuk LOG

    public static final int arab_size_small     = 16;
    public static final int indo_size_small     = 14;

    public static final int arab_size_medium    = 18;
    public static final int indo_size_medium    = 16;

    public static final int arab_size_large     = 22;
    public static final int indo_size_large     = 18;


    /*
    PR yng belum dikerjakan

    * jangan selalu open database
    * mengganti tab fokus ditengah
    * menambah delay

    */
}
