package com.ristekmuslim.mujamarob.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

/**
 * Created by imamudin on 21/01/19.
 */

public class DatabaseAccess2 {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess2 instance;

    private static String TABLE_ARAB3       = "quran";
    private static String COLUMN_ID         = "id";
    private static String COLUMN_ARAB       = "arab";
    private static String COLUMN_INDO       = "indonesia";
    private static String COLUMN_ARAB_NOHAR = "arabic_noharokah";

    private static String TABLE_ARAB_ARAB           = "mujamul_ghoni";
    private static String TABLE_ARAB_ARAB2          = "mujamul_muashiroh";
    private static String TABLE_ARAB_ARAB3          = "mujamul_wasith";
    private static String TABLE_ARAB_ARAB4          = "mujamul_muhith";
    private static String TABLE_ARAB_ARAB5          = "mujamul_shihah";
    private static String TABLE_ARAB_ARAB6          = "lisanularab";
    private static String TABLE_ARAB_ARAB7          = "ghoribulquran";
    private static String TABLE_ARAB8               = "quran";
    private static String ARABIC_WORD               = "arabic_word";
    private static String ARABIC_ROOT               = "arabic_root";
    private static String ARABIC_MEANINGS           = "arabic_meanings";
    private static String ARABIC_NOHAROKAH          = "arabic_noharokah";
    private static String ARABIC_NOHAROKAH2         = "word";

    private static final String[] arabicUnicode = {
            "\u0618","\u0619","\u061a",
            "\u064b","\u064c","\u064d","\u064e","\u064f","\u0650","\u0651",
            "\u0652","\u0653","\u0654","\u0655","\u0656","\u0657","\u0658",
            "\u0659","\u065a","\u065b","\u065c","\u065d","\u065e","\u065f"
    };
    private DatabaseAccess2(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }
    public static DatabaseAccess2 getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess2(context);
        }
        return instance;
    }
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }
    public void replace(){
        String replace = "";

        for(int i=0;i<arabicUnicode.length;i++){
            if(i==0)
                replace = "REPLACE("+COLUMN_ARAB +",'"+arabicUnicode[i]+"','')";
            else
                replace = "REPLACE("+replace +",'"+arabicUnicode[i]+"','')";
        }
        String selection = replace+" LIKE ?";

        Log.d("query",selection);
    }
    public static int is_tasdid(StringBuffer a){
        for(int i =0; i<a.length()-1;i++){
            char awal = a.charAt(i);
            if(awal == a.charAt(i + 1)){
                return i;
            }
        }
        return -1;
    }
    public static String hapus_harokah(String word){
        for(int i=0; i<arabicUnicode.length;i++){
            word = word.replace(arabicUnicode[i],"");
        }
        return  word;
    }
    public static boolean tasdid(StringBuffer a){
        for(int i =0; i<a.length();i++){
            if(a.charAt(i) == '\u0651'){
                return true;
            }
        }
        return false;
    }

    public ArrayList<QueryData> getWordsArab_MujamulGhoni(String word){
        word = hapus_harokah(word);
        char tasdid = '\u0651', alif='\u0627', alif_hamzah = '\u0623', alif_hamzah2 = '\u0625', alif_panjang='\u0622';
        StringBuffer word_sb = new StringBuffer(word);
        String s = "";
        Integer x = word.length();
        Integer y = x-1;

        if (word.indexOf(alif)!=-1) {
            s = s+" or  "+ARABIC_NOHAROKAH+" like '"+word.replace(alif,alif_hamzah)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH+" like '"+word.replace(alif,alif_hamzah2)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH+" like '"+word.replace(alif,alif_panjang)+"%' ";
         //   s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.replaceFirst("ال","")+"%' ";

        } else if ((word.charAt(0)=='ي' || word.charAt(0)=='م' || word.charAt(0)=='ت' ) && x>3){
            s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.substring(1)+"%' ";
        } else if (word.charAt(y)=='ة' && x>3){
            s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.substring(0,y)+"%' ";
        }
        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        //String query ="SELECT * FROM "+TABLE_ARAB_ARAB+" where "+ARABIC_NOHAROKAH+" like '"+word+"%' or "+ARABIC_NOHAROKAH+" like '"+word+" %' or "+ARABIC_ROOT +" like '"+word+"' "+s+" ORDER BY  LENGTH("+ARABIC_NOHAROKAH+") ASC";
        String query ="SELECT * FROM "+TABLE_ARAB_ARAB+" where "+ARABIC_NOHAROKAH+" like '%"+word+"%'  or "+ARABIC_ROOT +" like '%"+word+"%' "+s+" ORDER BY  LENGTH("+ARABIC_NOHAROKAH+") ASC";
        Log.i("query", query);
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            one = new QueryData();
            one.setArab(cursor.getString(1));
            one.setIndo(cursor.getString(3));
            data.add(one);
            cursor.moveToNext();
        }
        if (cursor.getCount()==0) {
            Log.i("query", query);
            one = new QueryData();
            one.setIndo("'<b>"+word+"</b>' not found");
            data.add(one);
        }
        cursor.close();
        return data;
    }

    public ArrayList<QueryData> getWordsArab_MujamulMuashiroh(String word){
        word = hapus_harokah(word);
        char tasdid = '\u0651', alif='\u0627', alif_hamzah = '\u0623', alif_hamzah2 = '\u0625', alif_panjang='\u0622';
        StringBuffer word_sb = new StringBuffer(word);
        String s = "";
        Integer x = word.length();
        Integer y = x-1;

        if (word.indexOf(alif)!=-1) {
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replace(alif,alif_hamzah)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replace(alif,alif_hamzah2)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replace(alif,alif_panjang)+"%' ";
           // s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replaceFirst("ال","")+"%' ";

        } else if ((word.charAt(0)=='ي' || word.charAt(0)=='م' || word.charAt(0)=='ت' ) && x>3){
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.substring(1)+"%' ";
        } else if (word.charAt(y)=='ة' && x>3){
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.substring(0,y)+"%' ";
        }
        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        String query ="SELECT * FROM "+TABLE_ARAB_ARAB2+" where "+ARABIC_NOHAROKAH2+" like '"+word+"%' or "+ARABIC_NOHAROKAH2+" like '%"+word+"%' "+s+" ORDER BY  LENGTH("+ARABIC_NOHAROKAH2+") ASC";
        Log.i("query", query);
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            one = new QueryData();
            one.setArab(cursor.getString(1));
            String arabic[] = (cursor.getString(2)).split(" ");
            String meaning="";
            arabic[0]="<font color='#C71585'><b>"+arabic[0]+"</b></font>";
            for(int i=0;i<arabic.length;i++) {
                meaning = meaning +" "+ arabic[i];
            }
            one.setIndo(meaning);
            data.add(one);
            cursor.moveToNext();
        }
        if (cursor.getCount()==0) {
            Log.i("query", query);
            one = new QueryData();
            one.setIndo("'<b>"+word+"</b>' not found");
            data.add(one);
        }
        cursor.close();
//        cursor2.close();
        return data;
    }

    public ArrayList<QueryData> getWordsArab_MujamulWasith(String word){
        word = hapus_harokah(word);
        char tasdid = '\u0651', alif='\u0627', alif_hamzah = '\u0623', alif_hamzah2 = '\u0625', alif_panjang='\u0622';
        StringBuffer word_sb = new StringBuffer(word);
        String s = "";
        Integer x = word.length();
        Integer y = x-1;

        if (word.indexOf(alif)!=-1) {
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replace(alif,alif_hamzah)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replace(alif,alif_hamzah2)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replace(alif,alif_panjang)+"%' ";
           // s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replaceFirst("ال","")+"%' ";

        } else if ((word.charAt(0)=='ي' || word.charAt(0)=='م' || word.charAt(0)=='ت' ) && x>3){
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.substring(1)+"%' ";
        } else if (word.charAt(y)=='ة' && x>3){
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.substring(0,y)+"%' ";
        }
        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        String query ="SELECT * FROM "+TABLE_ARAB_ARAB3+" where "+ARABIC_NOHAROKAH2+" like '"+word+"%' or "+ARABIC_NOHAROKAH2+" like '%"+word+"%' "+s+" ORDER BY  LENGTH("+ARABIC_NOHAROKAH2+") ASC";
        Log.i("query", query);
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            one = new QueryData();
            one.setArab(cursor.getString(1));
            String arabic[] = (cursor.getString(2)).split(" ");
            String meaning="";
            arabic[0]="<font color='#C71585'><b>"+arabic[0]+"</b></font>";
            for(int i=0;i<arabic.length;i++) {
                meaning = meaning +" "+ arabic[i];
            }
            one.setIndo(meaning);
            data.add(one);
            cursor.moveToNext();
        }
        if (cursor.getCount()==0) {
            Log.i("query", query);
            one = new QueryData();
            one.setIndo("'<b>"+word+"</b>' not found");
            data.add(one);
        }
        cursor.close();
//        cursor2.close();
        return data;
    }

    public ArrayList<QueryData> getWordsArab_MujamulMuhith(String word){
        word = hapus_harokah(word);
        char tasdid = '\u0651', alif='\u0627', alif_hamzah = '\u0623', alif_hamzah2 = '\u0625', alif_panjang='\u0622';
        StringBuffer word_sb = new StringBuffer(word);
        String s = "";
        Integer x = word.length();
        Integer y = x-1;

        if (word.indexOf(alif)!=-1) {
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replace(alif,alif_hamzah)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replace(alif,alif_hamzah2)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replace(alif,alif_panjang)+"%' ";
           // s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replaceFirst("ال","")+"%' ";

        } else if ((word.charAt(0)=='ي' || word.charAt(0)=='م' || word.charAt(0)=='ت' ) && x>3){
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.substring(1)+"%' ";
        } else if (word.charAt(y)=='ة' && x>3){
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.substring(0,y)+"%' ";
        }
        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        String query ="SELECT * FROM "+TABLE_ARAB_ARAB4+" where "+ARABIC_NOHAROKAH2+" like '"+word+"%' or "+ARABIC_NOHAROKAH2+" like '%"+word+"%' "+s+" ORDER BY  LENGTH("+ARABIC_NOHAROKAH2+") ASC";
        Log.i("query", query);
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            one = new QueryData();
            one.setArab(cursor.getString(1));
            String arabic[] = (cursor.getString(2)).split(" ");
            String meaning="";
            arabic[0]="<font color='#C71585'><b>"+arabic[0]+"</b></font>";
            for(int i=0;i<arabic.length;i++) {
                meaning = meaning +" "+ arabic[i];
            }
            one.setIndo(meaning);
            data.add(one);
            cursor.moveToNext();
        }
        if (cursor.getCount()==0) {
            Log.i("query", query);
            one = new QueryData();
            one.setIndo("'<b>"+word+"</b>' not found");
            data.add(one);
        }
        cursor.close();
//        cursor2.close();
        return data;
    }

    public ArrayList<QueryData> getWordsArab_MujamulShihah(String word){
        word = hapus_harokah(word);
        char tasdid = '\u0651', alif='\u0627', alif_hamzah = '\u0623', alif_hamzah2 = '\u0625', alif_panjang='\u0622';
        StringBuffer word_sb = new StringBuffer(word);
        String s = "";
        Integer x = word.length();
        Integer y = x-1;

        if (word.indexOf(alif)!=-1) {
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replace(alif,alif_hamzah)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replace(alif,alif_hamzah2)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replace(alif,alif_panjang)+"%' ";
           // s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.replaceFirst("ال","")+"%' ";

        } else if ((word.charAt(0)=='ي' || word.charAt(0)=='م' || word.charAt(0)=='ت' ) && x>3){
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.substring(1)+"%' ";
        } else if (word.charAt(y)=='ة' && x>3){
            s = s+" or  "+ARABIC_NOHAROKAH2+" like '%"+word.substring(0,y)+"%' ";
        }
        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        String query ="SELECT * FROM "+TABLE_ARAB_ARAB5+" where "+ARABIC_NOHAROKAH2+" like '"+word+"%' or "+ARABIC_NOHAROKAH2+" like '%"+word+"%' "+s+" ORDER BY  LENGTH("+ARABIC_NOHAROKAH2+") ASC";
        Log.i("query", query);
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            one = new QueryData();
            one.setArab(cursor.getString(1));
            String arabic[] = (cursor.getString(2)).split(" ");
            String meaning="";
            arabic[0]="<font color='#C71585'><b>"+arabic[0]+"</b></font>";
            for(int i=0;i<arabic.length;i++) {
                meaning = meaning +" "+ arabic[i];
            }
            one.setIndo(meaning);
            data.add(one);
            cursor.moveToNext();
        }
        if (cursor.getCount()==0) {
            Log.i("query", query);
            one = new QueryData();
            one.setIndo("'<b>"+word+"</b>' not found");
            data.add(one);
        }
        cursor.close();
//        cursor2.close();
        return data;
    }

    public ArrayList<QueryData> getWordsArab_LisanulArab(String word){
        word = hapus_harokah(word);
        char tasdid = '\u0651', alif='\u0627', alif_hamzah = '\u0623', alif_hamzah2 = '\u0625', alif_panjang='\u0622';
        StringBuffer word_sb = new StringBuffer(word);
        String s = "";
        Integer x = word.length();
        Integer y = x-1;

        if (word.indexOf(alif)!=-1) {
            s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.replace(alif,alif_hamzah)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.replace(alif,alif_hamzah2)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.replace(alif,alif_panjang)+"%' ";
            //s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.replaceFirst("ال","")+"%' ";

        } else if ((word.charAt(0)=='ي' || word.charAt(0)=='م' || word.charAt(0)=='ت' ) && x>3){
            s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.substring(1)+"%' ";
        } else if (word.charAt(y)=='ة' && x>3){
            s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.substring(0,y)+"%' ";
        }
        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        String query ="SELECT * FROM "+TABLE_ARAB_ARAB6+" where "+ARABIC_NOHAROKAH+" like '"+word+"%' or "+ARABIC_NOHAROKAH+" like '%"+word+"%' "+s+" ORDER BY  LENGTH("+ARABIC_NOHAROKAH+") ASC";
        Log.i("query", query);
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            one = new QueryData();
            one.setArab(cursor.getString(2));
            String arabic[] = (cursor.getString(1)).split(" ");
            String meaning="";
            arabic[0]="<font color='#C71585'><b>"+arabic[0]+"</b></font>";
            for(int i=0;i<arabic.length;i++) {
                meaning = meaning +" "+ arabic[i];
            }
            one.setIndo(meaning);
            data.add(one);
            cursor.moveToNext();
        }
        if (cursor.getCount()==0) {
            Log.i("query", query);
            one = new QueryData();
            one.setIndo("'<b>"+word+"</b>' not found");
            data.add(one);
        }
        cursor.close();
//        cursor2.close();
        return data;
    }

    public ArrayList<QueryData> getWordsArab_ghoribulquran(String word){
        word = hapus_harokah(word);
        char tasdid = '\u0651', alif='\u0627', alif_hamzah = '\u0623', alif_hamzah2 = '\u0625', alif_panjang='\u0622';
        StringBuffer word_sb = new StringBuffer(word);
        String s = "";

        if (word.indexOf(alif)!=-1) {
            s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.replace(alif,alif_hamzah)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.replace(alif,alif_hamzah2)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.replace(alif,alif_panjang)+"%' ";

        }
        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        String query ="SELECT * FROM "+TABLE_ARAB_ARAB7+" where "+ARABIC_NOHAROKAH+" like '%"+word+"%' "+s+" ORDER BY  LENGTH("+ARABIC_NOHAROKAH+") ASC";
        Log.i("query", query);
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            one = new QueryData();
            one.setArab(cursor.getString(1));
            one.setIndo(cursor.getString(2)+"<br/><small><i>("+cursor.getString(4)+":"+cursor.getString(6)+")</i></small>");
            data.add(one);
            cursor.moveToNext();
        }
        if (cursor.getCount()==0) {
            Log.i("query", query);
            one = new QueryData();
            one.setIndo("'<b>"+word+"</b>' not found");
            data.add(one);
        }
        cursor.close();
//        cursor2.close();
        return data;
    }

    public ArrayList<QueryData> getWordsArab_Quran(String word){
        word = hapus_harokah(word);
        Log.i("query", word);
        char tasdid = '\u0651', alif='\u0627', alif_hamzah = '\u0623', alif_hamzah2 = '\u0625', alif_panjang='\u0622';
        StringBuffer word_sb = new StringBuffer(word);
        String s  = "";
        String s2 = "";
        String s3 = "";

        if (word.indexOf(alif)!=-1) {
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah)+"' ";
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah2)+"' ";
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_panjang)+"' ";

            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah)+" %' ";
            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah2)+" %' ";
            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_panjang)+" %' ";

            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_hamzah)+"' ";
            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah2)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_hamzah2)+"' ";
            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_panjang)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_panjang)+"' ";
        }

        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        String query="";
        String query2="";
        Integer counter=0;
        Cursor cursor;

        String[] words= word.split(" ");
        if (words.length > 1) {
            query = "SELECT * FROM "+TABLE_ARAB8+" where "+COLUMN_ARAB_NOHAR+" like '%"+word+"%' "+s+" order by "+COLUMN_ID+"";
        }
        else {
            query ="SELECT * FROM "+ TABLE_ARAB8 +" where ("+COLUMN_ARAB_NOHAR+" like '"+word+"' or "+COLUMN_ARAB_NOHAR+" like '"+word+" %' or "+COLUMN_ARAB_NOHAR+" like '%والـ"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%ال"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '% "+word+"' or "+COLUMN_ARAB_NOHAR+" like '% "+word+" %' "+s+") ORDER BY "+COLUMN_ID+" ASC";
            query2 = "SELECT * FROM "+TABLE_ARAB8+" where "+COLUMN_ARAB_NOHAR+" like '%"+word+"%' "+s+" order by "+COLUMN_ID+"";
        }
        //if (word.length()>2){
        cursor = database.rawQuery(query, null);
        if (words.length<2 && cursor.getCount()==0) {
            cursor = database.rawQuery(query2, null);
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // if(!tmp.contains(cursor.getString(0))) {
            one = new QueryData();
            String tampung[] = (cursor.getString(4)).split(" ");
            String arabic[]  = (cursor.getString(3)).split(" ");
            String arabic_hasil="";
            Integer i;
            String quran = cursor.getString(3);
            if (tampung.length <= arabic.length && words.length<2 ){
                for (i=0;i<tampung.length;i++) {
                    if (tampung[i].contains(word)){
                        arabic[i]="<font color='#C71585'><b>"+arabic[i]+"</b></font>";
                    }
                }
                for(i=0;i<arabic.length;i++) {
                    arabic_hasil = arabic_hasil +" "+ arabic[i];
                }

                quran = arabic_hasil;
            } else if(words.length>1){
                String kalimah[]=word.split(" ");
                Integer start=0;
                Boolean finish=false;
                if (cursor.getString(4).contains(word)){
                    for (i=0;i<tampung.length;i++) {
                        if (i+1 < tampung.length) {
                            if(tampung[i].contains(kalimah[0]) && tampung[i+1].contains(kalimah[1]) && !finish){
                                arabic[i]="<font color='#C71585'><b>"+arabic[i];
                                start=i+kalimah.length-1;
                                finish=true;
                            }
                        }else if(tampung[i].contains(kalimah[0]) && !finish){
                            arabic[i]="<font color='#C71585'><b>"+arabic[i];
                            start=i+kalimah.length-1;
                            finish=true;
                        }
                    }
                    arabic[start]=arabic[start]+"</b></font>";
                }
                for(i=0;i<arabic.length;i++) {
                    arabic_hasil = arabic_hasil +" "+ arabic[i];
                }
                quran=arabic_hasil;

            } /* else if(words.length > 1) {
                            Integer index;
                            String ayah=cursor.getString(3);
                            index = cursor.getString(5).indexOf(word);
                            Log.i("result:",index+" "+ayah.length()+" "+word.length()+" "+cursor.getString(5).length());
                            if (index != -1){
                                if (index==0){
                                    quran="<font color='#064B7F'><b>"+ayah.substring(0,word.length())+"</b></font>"+ayah.substring(word.length()+1,ayah.length());
                                }else {
                                    quran = ayah.substring(0, index - 1) + "<font color='#064B7F'><b>" + ayah.substring(index, index + word.length()) + "</b></font>";
                                }
                                Log.i("ayah",quran);
                            }
                        }*/
            //one.setArab("\n"+"المختصر في تفسير : \n"+cursor.getString(7)+"\n"+"\n"+cursor.getString(4)+"\n");
            //one.setIndo(cursor.getString(3)+" ("+cursor.getString(6)+":"+cursor.getString(2)+") "+"\n");
            //one.setIndo(quran+" ("+cursor.getString(6)+":"+cursor.getString(2)+") "+"\n");

            one.setIndo("\n"+quran+"<br/>"+"<b> ("+cursor.getString(5)+":"+cursor.getString(2)+") </b>");
            one.setArab("<p>"+"<u>المختصر في تفسير :</u> <br/>"+cursor.getString(6)+"</p>");

            data.add(one);
            //}
            cursor.moveToNext();
        }
        // }

        if (cursor.getCount()==0) {
            one = new QueryData();
            one.setArab("'<b>"+word+"</b>' not found");
            data.add(one);
        }
        // }

        Log.i("query", query);
        Log.i("query2", query2);

        cursor.close();
        return data;
    }

    public ArrayList<QueryData> getWordsIndo_Quran(String word) {
        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        Cursor cursor;
        Integer counter=0;
        //Cursor cursor = database.rawQuery("SELECT * FROM "+TABLE_ARAB+" where "+COLUMN_INDO+" like '%"+word+"%' ORDER BY  LENGTH("+COLUMN_INDO+") ASC");
        //word="ka'bah";
        String query ="SELECT * FROM "+ TABLE_ARAB8 +" where "+COLUMN_INDO+" like ? ORDER BY "+COLUMN_ID+" ASC";
        String query2 ="SELECT * FROM "+ TABLE_ARAB8 +" where "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? ORDER BY "+COLUMN_ID+" ASC";


        cursor = database.rawQuery(query2, new String[] {word,word+" %","% "+word,"% "+word+" %"});
        counter=cursor.getCount();
        ArrayList<String> tmp = new ArrayList<String>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            one = new QueryData();
            tmp.add(cursor.getString(0));
            one.setArab("\n"+cursor.getString(3)+"<br/>"+" ("+cursor.getString(6)+":"+cursor.getString(2)+") " + "<p>"+"<u>المختصر في تفسير :</u> <br/>"+cursor.getString(7)+"</p>");
            one.setIndo("<p><i>"+cursor.getString(4).replaceAll("(?i)"+word,"<font color='#C71585'><b>"+word+"</b></font>")+"</i></p>");
            data.add(one);
            cursor.moveToNext();
        }
        if (cursor.getCount()<1) {
            cursor = database.rawQuery(query, new String[]{"%"+word+"%"});
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                if (!tmp.contains(cursor.getString(0))) {
                    one = new QueryData();
                    one.setArab("\n"+cursor.getString(3)+"<br/>"+" ("+cursor.getString(6)+":"+cursor.getString(2)+") " + "<p>"+"<u>المختصر في تفسير :</u> <br/>"+cursor.getString(7)+"</p>");
                    one.setIndo("<p><i>"+cursor.getString(4).replaceAll("(?i)" + word, "<font color='#C71585'>" + word + "</font>") + "</i></p>");
                    data.add(one);
                }
                cursor.moveToNext();
            }
        }
        if (cursor.getCount()==0 && counter==0) {
            one = new QueryData();
            one.setIndo("maaf kata '"+word+"' tidak ditemukan dalam kamus.\nsaran: silakan gunakan padanan kata yang lain atau mengurangi/menambah imbuhan pada kata tersebut\n\nUntuk mengaktifkan keyboard Arab baca catatan di menu About. Jika masih kesulitan menggunakan aplikasi silahkan kontak kami via WA di menu 'About' aplikasi");
            data.add(one);
        }
        Log.i("query", query);
        cursor.close();
        return data;
    }

    public static String delete_duplikat(String s) {
        return s.replaceAll("(.)(\\1)+", "$1");
    }
    public String saring_word(String word){
        word = delete_duplikat(word);
        if(word.charAt(0) =='\u0627'){                              //jika huruf awal alif

        }

        return word;
    }
    private class DatabaseOpenHelper extends SQLiteAssetHelper {
        private static final String DATABASE_NAME = "haramcopy1.db";
        private static final int DATABASE_VERSION = 2;

        public DatabaseOpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
    }
}