package com.ristekmuslim.mujamarob.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ristekmuslim.mujamarob.R;
import com.ristekmuslim.mujamarob.config.GlobalConfig;
import com.ristekmuslim.mujamarob.db.QueryData;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by agung on 03/09/2015.
 */
public class CustomListAdapterArab extends BaseAdapter {
    private ArrayList<QueryData> listData;
    private LayoutInflater layoutInflater;
    private Context context;

    public CustomListAdapterArab(Context aContext, ArrayList<QueryData> listData) {
        this.listData = listData;
        this.context = aContext;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_layout_arab, null);
            holder = new ViewHolder();
            holder.arab = (TextView) convertView.findViewById(R.id.tv_indo);
            holder.indo = (TextView) convertView.findViewById(R.id.tv_arab);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //untuk mengatur font size

        SharedPreferences mypref    = context.getSharedPreferences(GlobalConfig.NAMA_PREF, MODE_PRIVATE);

        int font_size = mypref.getInt(GlobalConfig.PREF_FONT_SIZE,0);

        //Log.d("font_size : ", ""+font_size);
        //pengaturan ukuran font dibalik , karena menampilkan date secara terbalik
        if(font_size == 0){
            holder.arab.setTextSize(GlobalConfig.indo_size_small);
            holder.indo.setTextSize(GlobalConfig.arab_size_small);

            //Log.d("font_size : ", "small : "+GlobalConfig.arab_size_small);
        }
        else if(font_size == 1){
            holder.arab.setTextSize(GlobalConfig.indo_size_medium);
            holder.indo.setTextSize(GlobalConfig.arab_size_medium);

            //Log.d("font_size : ", "medium :"+GlobalConfig.arab_size_medium);
        }else if(font_size == 2){
            holder.arab.setTextSize(GlobalConfig.indo_size_large);
            holder.indo.setTextSize(GlobalConfig.arab_size_large);

            //Log.d("font_size : ", "large : "+GlobalConfig.arab_size_large);
        }
        //tutup untuk mengatur font size

        if (listData.get(position).getIndo()!=null) {
            holder.indo.setText(Html.fromHtml(listData.get(position).getIndo()));
        } else{
            holder.indo.setText(listData.get(position).getIndo());
        }
        holder.arab.setText(Html.fromHtml(listData.get(position).getArab()));
        return convertView;
    }

    static class ViewHolder {
        TextView arab;
        TextView indo;
    }
}
