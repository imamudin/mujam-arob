package com.ristekmuslim.mujamarob;


import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.ristekmuslim.mujamarob.config.GlobalConfig;
import com.ristekmuslim.mujamarob.fragment.GhoribulQuran;
import com.ristekmuslim.mujamarob.fragment.LisanulArab;
import com.ristekmuslim.mujamarob.fragment.MujamulGhoni;
import com.ristekmuslim.mujamarob.fragment.MujamulMuashiroh;
import com.ristekmuslim.mujamarob.fragment.MujamulMuhith;
import com.ristekmuslim.mujamarob.fragment.MujamulShihah;
import com.ristekmuslim.mujamarob.fragment.MujamulWasith;
import com.ristekmuslim.mujamarob.fragment.Quran;

public class MainActivity extends AppCompatActivity {

    LinearLayout ll_main;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private static final int SHORT_DELAY = 1000; // 2 seconds

    //Fragments
    MujamulGhoni fMujamulGhoni;
    MujamulMuashiroh fMujamulMuashiroh;
    MujamulWasith fMujamulWasith;
    MujamulShihah fMujamulShihah;
    MujamulMuhith fMujamulMuhith;
    LisanulArab fLisanulArab;
    GhoribulQuran fGhoribulQuran;
    Quran fQuran;

    public SearchView searchView;
    String s_searchView ="";
    private char char_arab_terkecil = '\u0600', char_arab_terbesar ='\u06ff';
    private static String word="";

    SharedPreferences mypref;
    Boolean first_query = true;

    Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.tab_without_icon);
        setContentView(R.layout.main_activity);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);

        init();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {


            @Override
            public boolean onQueryTextSubmit(String query) {
                //action ketika pilih tombol search
                searchWord(query);
                s_searchView = query;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                s_searchView = newText;
                int sleep = 200;
                if (s_searchView.length() == 1)
                    sleep = 1000;
                else if (s_searchView.length() <= 3)
                    sleep = 500;
                else if (s_searchView.length() <= 5)
                    sleep = 400;
                else if (s_searchView.length() <= 7)
                    sleep = 300;
               // if(s_searchView.length()<=2) {
                    mHandler.removeCallbacksAndMessages(null);

                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Put your call to the server here (with mQueryString)
                            searchWord(s_searchView);
                        }
                    }, sleep);
                //} else {
                //    searchWord(s_searchView);
               // }

                return false;
            }
        });


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position,false);
                //openSnackBar(getString(R.string.refresh));
                if (position==0) {
                   if (s_searchView.length()>0)
                       searchWord(s_searchView);
                   actionBar.setTitle("معجم الغني");
                } else if (position==1) {
                    if (s_searchView.length()>0)
                        searchWord(s_searchView);
                    actionBar.setTitle("معجم اللغة العربية المعاصرة");
                } else if (position==2) {
                    if (s_searchView.length()>0)
                        searchWord(s_searchView);
                    actionBar.setTitle("معجم الوسيط");
                } else if (position==3) {
                    if (s_searchView.length()>0)
                        searchWord(s_searchView);
                    actionBar.setTitle("القاموس المحيط");
                } else if (position==4) {
                    if (s_searchView.length()>0)
                        searchWord(s_searchView);
                    actionBar.setTitle("مختار الصحاح");
                } else if (position==5) {
                    if (s_searchView.length()>0)
                        searchWord(s_searchView);
                    actionBar.setTitle("لسان العرب");
                } else if (position==6) {
                    if (s_searchView.length()>0)
                        searchWord(s_searchView);
                    actionBar.setTitle("غريب القرآن");
                } else if (position==7) {
                    if (s_searchView.length()>0)
                        searchWord(s_searchView);
                    actionBar.setTitle("المختصر في تفسير القرآن الكريم");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //setupViewPager viewPager;
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        fMujamulGhoni = new MujamulGhoni();
        fMujamulMuashiroh = new MujamulMuashiroh();
        fMujamulWasith = new MujamulWasith();
        fMujamulMuhith = new MujamulMuhith();
        fMujamulShihah = new MujamulShihah();
        fLisanulArab = new LisanulArab();
        fGhoribulQuran = new GhoribulQuran();
        fQuran = new Quran();

        adapter.addFragment(fMujamulGhoni,"معجم الغني");
        adapter.addFragment(fMujamulMuashiroh,"معجم المعاصرة");
        adapter.addFragment(fMujamulWasith,"المعجم الوسيط");
        adapter.addFragment(fMujamulMuhith,"القاموس المحيط");
        adapter.addFragment(fMujamulShihah,"مختار الصحاح");
        adapter.addFragment(fLisanulArab,"لسان العرب");
        adapter.addFragment(fGhoribulQuran,"غريب القرآن");
        adapter.addFragment(fQuran,"تفسير القرآن");

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);

        //mengatur icon tab
        tabLayout.getTabAt(0).setIcon(R.drawable.ghoin);
        tabLayout.getTabAt(1).setIcon(R.drawable.ain);
        tabLayout.getTabAt(2).setIcon(R.drawable.waw);
        tabLayout.getTabAt(3).setIcon(R.drawable.mim);
        tabLayout.getTabAt(4).setIcon(R.drawable.shad);
        tabLayout.getTabAt(5).setIcon(R.drawable.lam);
        tabLayout.getTabAt(6).setIcon(R.drawable.sin);
        tabLayout.getTabAt(7).setIcon(R.drawable.quran);

        tabLayout.getTabAt(0).getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTextPrimary), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(1).getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTextDisable), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(2).getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTextDisable), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(3).getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTextDisable), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(4).getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTextDisable), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(5).getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTextDisable), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(6).getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTextDisable), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(7).getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTextDisable), PorterDuff.Mode.SRC_IN);

        tabLayout.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {

                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);
                        int tabIconColor = ContextCompat.getColor(getApplicationContext(), R.color.colorTextPrimary);
                        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                        int tabIconColor = ContextCompat.getColor(getApplicationContext(), R.color.colorTextDisable);
                        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }
                }
        );

        Toast.makeText(getApplicationContext(),"Slide left or right to change another dictionary", Toast.LENGTH_SHORT).show();

    }
    private boolean isArab(char x){
        if(x > char_arab_terkecil){
            return  true;
        }
        return false;
    }
    private void searchWord(String newText){
        Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + viewPager.getCurrentItem());
        if (viewPager.getCurrentItem() == 0 && page != null) {  //untuk mujamul ghoni
            ((MujamulGhoni)page).callDatas(newText);
        }else if (viewPager.getCurrentItem() == 1 && page != null) { //untuk mujamul muashiroh
            ((MujamulMuashiroh)page).callDatas(newText);
        }else if (viewPager.getCurrentItem() == 2 && page != null) { //untuk mujamul wasith
            ((MujamulWasith)page).callDatas(newText);
        }else if (viewPager.getCurrentItem() == 3 && page != null) { //untuk mujamul muhith
            ((MujamulMuhith)page).callDatas(newText);
        }else if (viewPager.getCurrentItem() == 4 && page != null) { //untuk mukhtar alshihah
            ((MujamulShihah)page).callDatas(newText);
        }else if (viewPager.getCurrentItem() == 5 && page != null) { //untuk lisanul arab
            ((LisanulArab)page).callDatas(newText);
        }else if (viewPager.getCurrentItem() == 6 && page != null) { //untuk ghoribul quran
            ((GhoribulQuran)page).callDatas(newText);
        }else if (viewPager.getCurrentItem() == 7 && page != null) { //untuk tafsir quran
            ((Quran)page).callDatas(newText);
        }
    }
    private void init(){
        ll_main     = (LinearLayout)findViewById(R.id.ll_main);
        mypref      = getSharedPreferences(GlobalConfig.NAMA_PREF, MODE_PRIVATE);

        first_query = mypref.getBoolean(GlobalConfig.PREF_FIRST_QUERY,true);

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);

        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);

        searchView  = (SearchView) findViewById(R.id.searchView);

        mHandler =  new Handler();

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        // Associate searchable configuration with the SearchView
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        int id = item.getItemId();
        switch (item.getItemId()) {
            case R.id.action_mujamulghoni:
                viewPager.setCurrentItem(0);
                return true;
            case R.id.action_mujamulmuashiroh:
                viewPager.setCurrentItem(1);
                return true;
            case R.id.action_mujamulwasith:
                viewPager.setCurrentItem(2);
                return true;
            case R.id.action_mujamulmuhith:
                viewPager.setCurrentItem(3);
                return true;
            case R.id.action_mujamulshihah:
                viewPager.setCurrentItem(4);
                return true;
            case R.id.action_lisanularab:
                viewPager.setCurrentItem(5);
                return true;
            case R.id.action_ghoribulquran:
                viewPager.setCurrentItem(6);
                return true;
            case R.id.action_quran:
                viewPager.setCurrentItem(7);
                return true;
            case R.id.action_about:
                show_about();
                return true;
            case R.id.action_rate:
                rate();
                return true;
            case R.id.action_share:
                share();
                return true;
            case R.id.action_setting:
                show_setting();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void openSnackBar(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_SHORT);
        snack.setActionTextColor(getResources().getColor(android.R.color.white ))
                .setActionTextColor(getResources().getColor(R.color.colorPrimary))
                .setAction("Refresh", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        searchWord(s_searchView);
                    }
                })
                .show();
    }
    private void notifFirstQuery(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_SHORT);
        snack.setActionTextColor(getResources().getColor(android.R.color.holo_green_light ))
                .setActionTextColor(getResources().getColor(R.color.colorPrimary))
                .show();
    }

    private void check_updates(){
        Intent url_app=new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id="+getPackageName()));
        startActivity(url_app);
    }

    private void show_setting(){
        String title = ""+getResources().getString(R.string.app_name);
        LayoutInflater layoutInflater = (LayoutInflater)this.getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.dialog_font_setting, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title      = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final RadioGroup radioGroup = (RadioGroup) promptView.findViewById(R.id.rg_font_setting);

        ((RadioButton)radioGroup.getChildAt(mypref.getInt(GlobalConfig.PREF_FONT_SIZE,0))).setChecked(true);

        t_title.setText("Pilih Ukuran Font");

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setNegativeButton("Kembali",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int radioButtonID = radioGroup.getCheckedRadioButtonId();
                        View radioButton = radioGroup.findViewById(radioButtonID);
                        int idx = radioGroup.indexOfChild(radioButton);

                        searchWord(s_searchView);
                        //Toast.makeText(MainActivity.this, "id : "+idx, Toast.LENGTH_SHORT).show();

                        //menyimpan ke database first query
                        SharedPreferences.Editor editor = mypref.edit();
                        editor.putInt(GlobalConfig.PREF_FONT_SIZE, idx).commit();

                        Toast toast = new Toast(getApplicationContext());
                        toast.setDuration(Toast.LENGTH_SHORT);

                        String font_type="";
                        if (idx==0) {
                            font_type="NORMAL";
                        } else if (idx==1){
                            font_type="SEDANG";
                        } else if (idx==2){
                            font_type="BESAR";
                        }

                        //inflate view
                        View custom_view = getLayoutInflater().inflate(R.layout.toast_icon_text, null);
                        ((TextView) custom_view.findViewById(R.id.message)).setText("Ukuran Font : "+font_type);
                        ((ImageView) custom_view.findViewById(R.id.icon)).setImageResource(R.drawable.ic_done);

                        toast.setView(custom_view);
                        toast.show();
                    }
                });
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        alert.show();
    }


    private void show_about(){
        String title = ""+getResources().getString(R.string.app_name);
        String text = "معجم العرب : <br>"+
                "<br>\n" +
                "١. معجم الغني" +
                "<br>\n" +
                "٢. معجم اللغة العربية المعاصرة" +
                "<br>\n" +
                "٣. المعجم الوسيط" +
                "<br>\n" +
                "٤. القاموس المحيط" +
                "<br>\n" +
                "٥. مختار الصحاح" +
                "<br>\n" +
                "٦. لسان العرب" +
                "<br>\n" +
                "٧. السراج في بيان غريب القرآن " +
                "<br>\n" +
                "٨. المختصر في تفسير القرآن الكريم" +
                "<br>\n" +
                "<br>\n" +
                "Contact Us : <a href=\"mailto:android@ristekmuslim.com\" target=\"_top\">android@ristekmuslim.com</a><br>\n" +
                "<b>Ristek Muslim</b>";

        LayoutInflater layoutInflater = (LayoutInflater)this.getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.notif_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title  = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final TextView t_text   = (TextView) promptView.findViewById(R.id.t_text_dialog);

        t_title.setText(title);
        t_text.setText(Html.fromHtml(text));
        t_text.setMovementMethod(LinkMovementMethod.getInstance());
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setNegativeButton("Tutup",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public void rate(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id="+getPackageName())));
        }
    }
    public void share(){
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Free Arabic Dictionary معجم العرب \nhttp://play.google.com/store/apps/details?id="+getPackageName());
        startActivity(Intent.createChooser(shareIntent, "Share..."));
    }

}
