package com.ristekmuslim.mujamarob;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by agung on 16/12/2015.
 */
public class DetailWord extends AppCompatActivity {
    TextView tv_arab_arab, tv_arab_indo, tv_indo_indo, tv_indo_arab;
    LinearLayout ll_arab, ll_indo;
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent myLocalIntent = getIntent();

        Bundle myBundle = myLocalIntent.getExtras();
        String arab     = myBundle.getString("arab");
        String indo     = myBundle.getString("indo");
        Boolean isArab  = myBundle.getBoolean("isArab");

        setContentView(R.layout.detail_kata);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("" + getResources().getString(R.string.app_name));
        //actionBar.setIcon(R.drawable.action_bar);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);

        if(isArab){
            ll_indo = (LinearLayout)findViewById(R.id.ll_indo);
            ll_indo.setVisibility(View.GONE);
            tv_arab_arab    = (TextView)findViewById(R.id.tv_arab_arab);
            tv_arab_indo    = (TextView)findViewById(R.id.tv_arab_indo);

            tv_arab_arab.setText(Html.fromHtml(indo));
            arab = arab.replace("|", "<br/>");
            tv_arab_indo.setText(Html.fromHtml(arab));
        }else{
            ll_arab = (LinearLayout)findViewById(R.id.ll_arab);
            ll_arab.setVisibility(View.GONE);
            tv_indo_arab    = (TextView)findViewById(R.id.tv_indo_arab);
            tv_indo_indo    = (TextView)findViewById(R.id.tv_indo_indo);

            tv_indo_arab.setText(Html.fromHtml(arab));
            tv_indo_indo.setText(Html.fromHtml(indo));
        }
    }

    public void copyAction(View view) {

        Intent myLocalIntent = getIntent();

        Bundle myBundle = myLocalIntent.getExtras();
        String arab="";
        String indo="";

        Boolean isArab  = myBundle.getBoolean("isArab");
        if (!isArab){
            arab = Html.fromHtml(myBundle.getString("arab")).toString();
            indo = Html.fromHtml(myBundle.getString("indo")).toString();
        } else {
            indo = Html.fromHtml(myBundle.getString("arab")).toString();
            arab = Html.fromHtml(myBundle.getString("indo")).toString();
        }


        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("kamus", arab+" : "+indo);
        clipboard.setPrimaryClip(clip);


        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
//
//        //inflate view
        View custom_view = getLayoutInflater().inflate(R.layout.toast_icon_text, null);
        ((TextView) custom_view.findViewById(R.id.message)).setText("Text Copied");
        ((ImageView) custom_view.findViewById(R.id.icon)).setImageResource(R.drawable.ic_done);
//
        toast.setView(custom_view);
        toast.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
//            case R.id.action_permohonan:
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
