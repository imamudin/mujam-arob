package com.ristekmuslim.mujamarob.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ristekmuslim.mujamarob.DetailWord;
import com.ristekmuslim.mujamarob.R;
import com.ristekmuslim.mujamarob.adapter.CustomListAdapterLisanulArab;
import com.ristekmuslim.mujamarob.db.DatabaseAccess2;
import com.ristekmuslim.mujamarob.db.QueryData;

import java.util.ArrayList;

/**
 * Created by imamudin on 17/01/19.
 */

public class MujamulShihah extends Fragment {

    private ArrayList ar_words = new ArrayList();
    private ArrayList data, wordsArab;
    private ListView list_query;
    private CustomListAdapterLisanulArab adapter_arab;

    private char char_arab_terkecil = '\u0600', char_arab_terbesar ='\u06ff';
    View rootView;
    private SwipeRefreshLayout swipeContainer;
    String word="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        rootView = inflater.inflate(R.layout.lisanul_arob, container, false);

        rootView = inflater.inflate(R.layout.layout_listview, container, false);

        swipeContainer = (SwipeRefreshLayout)rootView.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeResources(
                R.color.colorPrimary);
        return rootView;
    }

    public void callDatas(String word){
        new AsyncTaskRunner().execute(word);
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, ArrayList> {

        @Override
        protected ArrayList doInBackground(String... params) {
            //publishProgress("Sleeping..."); // Calls onProgressUpdate()
            word = params[0];

            if (!word.equals("") && word.length() > 0) {
                DatabaseAccess2 databaseAccess2 = DatabaseAccess2.getInstance(getContext());
                databaseAccess2.open();
                final boolean isArab = isArab(word.toLowerCase().charAt(0));

                if(!isArab){
                    ar_words = new ArrayList<QueryData>();
                    QueryData one = new QueryData();
                    one.setIndo("'<b>"+word+"</b>' not found");
                    ar_words.add(one);
                }else {
                    ar_words = databaseAccess2.getWordsArab_MujamulShihah(word.replace("'",""));
                }
                databaseAccess2.close();
            } else {
                ar_words = new ArrayList<QueryData>();
            }
            return ar_words;
        }

        @Override
        protected void onPostExecute(ArrayList ar_words) {
            // execution of result of Long time consuming operation
            if (word.length() > 0) {
                final boolean isArab = isArab(word.toLowerCase().charAt(0));
                list_query = (ListView)rootView.findViewById(R.id.lv_result);
                adapter_arab = new CustomListAdapterLisanulArab(getContext(), ar_words);

                list_query.setAdapter(adapter_arab);

                registerForContextMenu(list_query);
                list_query.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                        Object o = list_query.getItemAtPosition(position);
                        QueryData newsData = (QueryData) o;

                        Intent myIntentA1A2 = new Intent(getContext(), DetailWord.class);
                        Bundle myData = new Bundle();

                        myData.putBoolean("isArab", isArab);
                        String sentence = newsData.getIndo();
                        sentence = sentence.replace("\\r\\n", "\n");
                        myData.putString("arab", "" + sentence);
                        myData.putString("indo", newsData.getArab());

                        myIntentA1A2.putExtras(myData);

                        startActivityForResult(myIntentA1A2, 103);
                    }
                });
                adapter_arab.notifyDataSetChanged();
            } else {
                list_query = (ListView)rootView.findViewById(R.id.lv_result);
                adapter_arab = new CustomListAdapterLisanulArab(getContext(), ar_words);

                list_query.setAdapter(adapter_arab);
                registerForContextMenu(list_query);
                adapter_arab.notifyDataSetChanged();
            }
            swipeContainer.setRefreshing(false);
        }


        @Override
        protected void onPreExecute() {
            swipeContainer.setRefreshing(true);
        }


        @Override
        protected void onProgressUpdate(String... text) {
//            finalResult.setText(text[0]);

        }
    }
    /*
    public void callDatasx(String word){

        if (!word.equals("")) {
            DatabaseAccess2 databaseAccess2 = DatabaseAccess2.getInstance(getContext());
            databaseAccess2.open();

            final boolean isArab = isArab(word.toLowerCase().charAt(0));

            if(isArab){
                wordsArab = databaseAccess2.getWordsArab_MujamulShihah(word);
                list_query = (ListView)rootView.findViewById(R.id.lv_result);
                adapter_arab = new CustomListAdapterLisanulArab(getContext(), wordsArab);

                list_query.setAdapter(adapter_arab);

                registerForContextMenu(list_query);
                list_query.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                        Object o = list_query.getItemAtPosition(position);
                        QueryData newsData = (QueryData) o;

                        Intent myIntentA1A2 = new Intent(getContext(), DetailWord.class);
                        Bundle myData = new Bundle();

                        myData.putBoolean("isArab", isArab);
                        String sentence = newsData.getIndo();
                        sentence = sentence.replace("\\r\\n", "\n");
                        myData.putString("arab", "" + sentence);
                        myData.putString("indo", newsData.getArab());

                        myIntentA1A2.putExtras(myData);

                        startActivityForResult(myIntentA1A2, 103);
                    }
                });
                adapter_arab.notifyDataSetChanged();
            }else{
                data = new ArrayList<QueryData>();

                QueryData one = new QueryData();
                one.setIndo("'<b>"+word+"</b>' not found");
                data.add(one);

                list_query = (ListView)rootView.findViewById(R.id.lv_result);
                adapter_arab = new CustomListAdapterLisanulArab(getContext(), data);

                list_query.setAdapter(adapter_arab);
                registerForContextMenu(list_query);
                adapter_arab.notifyDataSetChanged();
            }
            databaseAccess2.close();
        } else {
            data = new ArrayList<QueryData>();
            list_query = (ListView)rootView.findViewById(R.id.lv_result);
            adapter_arab = new CustomListAdapterLisanulArab(getContext(), data);

            list_query.setAdapter(adapter_arab);
            registerForContextMenu(list_query);
            adapter_arab.notifyDataSetChanged();
        }
    }
    */
    private boolean isArab(char x){
        if(x > char_arab_terkecil){
            return  true;
        }
        return false;
    }
}